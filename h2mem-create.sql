create table car_info (
  id                        bigint auto_increment not null,
  make                      varchar(100) not null,
  model                     varchar(100) not null,
  year                      integer,
  constraint pk_car_info primary key (id))
;



