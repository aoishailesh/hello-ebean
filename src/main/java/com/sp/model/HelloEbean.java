package com.sp.model;

import java.util.List;

import org.avaje.agentloader.AgentLoader;

import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.EbeanServerFactory;

/**
 * 
 * Driver to test Ebean use
 * 
 * @author shailesh.patel
 */
public class HelloEbean {

	public static void main(String[] args) {
		setupEbean();
		
		System.out.println("Creating car...");
		CarInfo ci = new CarInfo();
		ci.setMake("Toyota");
		ci.setModel("Camry");
		ci.setYear(2007);
		
		EbeanServer ebserver = EbeanServerFactory.create("h2mem");
		ebserver.save(ci);
		
		List<CarInfo> results = ebserver.createQuery(CarInfo.class).findList();
		
		System.out.println("results: " + results);
		for (CarInfo rci : results) {
			System.out.println("found car: " + rci);
		}
	}
	
	public static void setupEbean() {

		// start the Ebean ORM agent to enhance all the entity objects for ORM
		AgentLoader.loadAgentFromClasspath("avaje-ebeanorm-agent", "debug=1");
		
	}
}
